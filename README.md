# M242 - Mikroprozessoranwendung realisieren

Kompetenz Mikroprozessoranwendung nach Vorgabe strukturieren,
programmieren, auf der Zielhardware implementieren und testen.

Handlungsziele
1. Vorgabe auf verlangte Funktionen und benötigte Input- und
Outputdaten/-Signale analysieren.
2. Zeitkritische und zeitlich unabhängige Funktionen
identifizieren und in einem Programmentwurf logisch
gliedern, Datentypen den Daten/Signalen zuordnen.
3. Mittels einer Programmiersprache und
Programmierumgebung den Programmentwurf umsetzen.
4. Aus der Vorgabe die Testfälle identifizieren, spezifizieren und
dokumentieren.
5. Realisierte Applikation mit geeigneten Werkzeugen auf der
Zielhardware austesten, Fehler identifizieren, korrigieren und
dokumentieren.

Kompetenzfeld: Technical SW Engineering
Objekt: Hardwarenahe Mikroprozessoranwendung
Niveau: 2
Voraussetzungen: Programmieren nach Vorgabe
Anzahl Lektionen: 40
Anerkennung: Eidg. Fähigkeitszeugnis
